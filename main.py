from flask import Flask, json
from flask import jsonify
from config import config
from models import db
from models import Usuario, Pais, Usuario_tiene_moneda, Cuenta_bancaria, Moneda, Precio_moneda
from flask import request

def create_app(enviroment):
	app = Flask(__name__)
	app.config.from_object(enviroment)
	with app.app_context():
		db.init_app(app)
		db.create_all()
	return app

# Accedemos a la clase config del archivo config.py
enviroment = config['development']
app = create_app(enviroment)

###########################
#Endpoints para tabla pais#
###########################
@app.route('/api/pais', methods=['POST'])
def create_pais():
	json = request.get_json(force=True)
	if (json.get('cod_pais') is None) or (json.get('nombre') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	pais = Pais.create(json['cod_pais'], json['nombre'])
	return jsonify({'Pais': pais.json()})

@app.route('/api/pais', methods=['GET'])
def get_paises():
	paises = [pais.json() for pais in Pais.query.all()]
	return jsonify({'Paises': paises})

@app.route('/api/pais/<cod_pais>', methods=['GET'])
def get_pais(cod_pais):
	pais = Pais.query.filter_by(cod_pais=cod_pais).first()
	if pais is None:
		return jsonify({'message': 'El pais no existe'}), 404
	return jsonify({'Pais': pais.json()})	

@app.route('/api/pais/<cod_pais>', methods=['PUT'])
def update_pais(cod_pais):
	pais = Pais.query.filter_by(cod_pais=cod_pais).first()
	if pais is None:
		return jsonify({'message': 'El pais no existe'}), 404
	json = request.get_json(force=True)
	if json.get('nombre') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	pais.nombre = json['nombre']
	pais.update()
	return jsonify({'Pais': pais.json()})

@app.route('/api/pais/<cod_pais>', methods=['DELETE'])
def delete_pais(cod_pais):
	pais = Pais.query.filter_by(cod_pais=cod_pais).first()
	if pais is None:
		return jsonify({'message': 'El pais no existe'}), 404
	pais.delete()
	return jsonify({'Pais eliminado': pais.json()})


##############################
#Endpoints para tabla usuario#
##############################
@app.route('/api/usuario', methods=['POST'])
def create_usuario():
	json = request.get_json(force=True)
	if (json.get('nombre') is None) or (json.get('correo') is None) or (json.get('contraseña') is None) or (json.get('pais') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	if json.get('apellido') is None:
		usuario = Usuario.create(json['nombre'], json['correo'], json['contraseña'], json['pais'])
		return jsonify({'Usuario': usuario.json()})
	usuario = Usuario.create(json['nombre'], json['correo'], json['contraseña'], json['pais'], json['apellido'])
	return jsonify({'Usuario': usuario.json()})
	
@app.route('/api/usuario', methods=['GET'])
def get_usuarios():
	usuarios = [usuario.json() for usuario in Usuario.query.all()]
	return jsonify({'Usuarios': usuarios})

@app.route('/api/usuario/<id>', methods=['GET'])
def get_usuario(id):
	usuario = Usuario.query.filter_by(id=id).first()
	if usuario is None:
		return jsonify({'message': 'El usuario no existe'}), 404
	return jsonify({'Usuario': usuario.json()})

@app.route('/api/usuario/<id>', methods=['PUT'])
def update_usuario(id):
	usuario = Usuario.query.filter_by(id=id).first()
	if usuario is None:
		return jsonify({'message': 'El usuario no existe'}), 404
	json = request.get_json(force=True)
	if (json.get('nombre') is None) or (json.get('correo') is None) or (json.get('contraseña') is None) or (json.get('pais') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	usuario.nombre = json['nombre']
	usuario.apellido = None if json.get('apellido') is None else json['apellido']
	usuario.correo = json['correo']
	usuario.contraseña = json['contraseña']
	usuario.pais = json['pais']
	usuario.update()
	return jsonify({'Usuario': usuario.json()})
	
@app.route('/api/usuario/<id>', methods=['DELETE'])
def delete_usuario(id):
	usuario = Usuario.query.filter_by(id=id).first()
	if usuario is None:
		return jsonify({'message': 'El usuario no existe'}), 404
	usuario.delete()
	return jsonify({'Usuario eliminado': usuario.json()})


###########################################
#Endpoints para tabla usuario_tiene_moneda#
###########################################
@app.route('/api/usuario_tiene_moneda', methods=['POST'])
def create_usuario_tiene_moneda():
	json = request.get_json(force=True)
	if (json.get('id_usuario') is None) or (json.get('id_moneda') is None) or (json.get('balance') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	usuario_tiene_moneda = Usuario_tiene_moneda.create(json['id_usuario'], json['id_moneda'], json['balance'])
	return jsonify({'Usuario': usuario_tiene_moneda.json()})

@app.route('/api/usuario_tiene_moneda', methods=['GET'])
def get_usuarios_tiene_monedas():
	usuarios_tienen_monedas = [usuario_tiene_moneda.json() for usuario_tiene_moneda in Usuario_tiene_moneda.query.all()]
	return jsonify({'Usuarios': usuarios_tienen_monedas})

@app.route('/api/usuario_tiene_moneda/<id_usuario>&<id_moneda>', methods=['GET'])
def get_usuario_tiene_moneda(id_usuario, id_moneda):
	usuario_tiene_moneda = Usuario_tiene_moneda.query.filter_by(id_usuario=id_usuario).filter_by(id_moneda=id_moneda).first()
	if usuario_tiene_moneda is None:
		return jsonify({'message': 'El usuario no existe o no tiene esa moneda'}), 404
	return jsonify({'Usuario': usuario_tiene_moneda.json()})

@app.route('/api/usuario_tiene_moneda/<id_usuario>&<id_moneda>', methods=['PUT'])
def update_usuario_tiene_moneda(id_usuario, id_moneda):
	usuario_tiene_moneda = Usuario_tiene_moneda.query.filter_by(id_usuario=id_usuario).filter_by(id_moneda=id_moneda).first()
	if usuario_tiene_moneda is None:
		return jsonify({'message': 'El usuario no existe o no tiene esa moneda'}), 404
	json = request.get_json(force=True)
	if json.get('balance') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	usuario_tiene_moneda.balance = json['balance']
	usuario_tiene_moneda.update()
	return jsonify({'Usuario': usuario_tiene_moneda.json()})

@app.route('/api/usuario_tiene_moneda/<id_usuario>&<id_moneda>', methods=['DELETE'])
def delete_usuario_tiene_moneda(id_usuario, id_moneda):
	usuario_tiene_moneda = Usuario_tiene_moneda.query.filter_by(id_usuario=id_usuario).filter_by(id_moneda=id_moneda).first()
	if usuario_tiene_moneda is None:
		return jsonify({'message': 'El usuario no existe o no tiene esa moneda'}), 404
	usuario_tiene_moneda.delete()
	return jsonify({'Usuario eliminado': usuario_tiene_moneda.json()})

######################################
#Endpoints para tabla cuenta_bancaria#
######################################
@app.route('/api/cuenta_bancaria', methods=['POST'])
def create_cuenta_bancaria():
	json = request.get_json(force=True)
	if (json.get('numero_cuenta') is None) or (json.get('id_usuario') is None) or (json.get('balance') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	cuenta_bancaria = Cuenta_bancaria.create(json['numero_cuenta'], json['id_usuario'], json['balance'])
	return jsonify({'Cuenta bancaria': cuenta_bancaria.json()})

@app.route('/api/cuenta_bancaria', methods=['GET'])
def get_cuentas_bancarias():
	cuentas_bancarias = [cuenta_bancaria.json() for cuenta_bancaria in Cuenta_bancaria.query.all()]
	return jsonify({'Cuentas bancarias': cuentas_bancarias})

@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['GET'])
def get_cuenta_bancaria(numero_cuenta):
	cuenta_bancaria = Cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta_bancaria is None:
		return jsonify({'message': 'La cuenta bancaria no existe'}), 404
	return jsonify({'Cuenta bancaria': cuenta_bancaria.json()})

@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['PUT'])
def update_cuenta_bancaria(numero_cuenta):
	cuenta_bancaria = Cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta_bancaria is None:
		return jsonify({'message': 'La cuenta bancaria no existe'}), 404
	json = request.get_json(force=True)
	if (json.get('id_usuario') is None) or (json.get('balance') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	cuenta_bancaria.id_usuario = json['id_usuario']
	cuenta_bancaria.balance = json['balance']
	cuenta_bancaria.update()
	return jsonify({'Cuenta bancaria': cuenta_bancaria.json()})

@app.route('/api/cuenta_bancaria/<numero_cuenta>', methods=['DELETE'])
def delete_cuenta_bancaria(numero_cuenta):
	cuenta_bancaria = Cuenta_bancaria.query.filter_by(numero_cuenta=numero_cuenta).first()
	if cuenta_bancaria is None:
		return jsonify({'message': 'La cuenta bancaria no existe'}), 404
	cuenta_bancaria.delete()
	return jsonify({'Cuenta bancaria eliminada': cuenta_bancaria.json()})

#############################
#Endpoints para tabla moneda#
#############################
@app.route('/api/moneda', methods=['POST'])
def create_moneda():
	json = request.get_json(force=True)
	if (json.get('id') is None) or (json.get('sigla') is None) or (json.get('nombre') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	moneda = Moneda.create(json['id'], json['sigla'], json['nombre'])
	return jsonify({'Moneda': moneda.json()})

@app.route('/api/moneda', methods=['GET'])
def get_monedas():
	monedas = [moneda.json() for moneda in Moneda.query.all()]
	return jsonify({"monedas": monedas})

@app.route('/api/moneda/<id>', methods=['GET'])
def get_moneda(id):
	moneda = Moneda.query.filter_by(id=id).first()
	if moneda is None:
		return jsonify({"message": 'Moneda no existe'}), 404
	return jsonify({'Moneda': moneda.json()})

@app.route('/api/moneda/<id>', methods=['PUT'])
def update_moneda(id):
	moneda = Moneda.query.filter_by(id=id).first()
	if moneda is None:
		return jsonify({"message": 'Moneda no existe'}), 404
	json = request.get_json(force=True)
	if (json.get('sigla') is None) or (json.get('nombre') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	moneda.sigla = json['sigla']
	moneda.nombre = json['nombre']
	moneda.update()
	return jsonify({'Moneda': moneda.json()})
	
@app.route('/api/moneda/<id>', methods=['DELETE'])
def delete_moneda(id):
	moneda = Moneda.query.filter_by(id=id).first()
	if moneda is None:
		return jsonify({'message': 'La moneda no existe'}), 404
	moneda.delete()
	return jsonify({'Moneda': moneda.json()})

####################################
#Endpoints para tabla precio_moneda#
####################################
@app.route('/api/precio_moneda', methods=['POST'])
def create_precio_moneda():
	json = request.get_json(force=True)
	if (json.get('id_moneda') is None) or (json.get('valor') is None):
		return jsonify({'message': 'El formato está mal'}), 400
	precio_moneda = Precio_moneda.create(json['id_moneda'], json['valor'])
	return jsonify({'Precio moneda': precio_moneda.json()})

@app.route('/api/precio_moneda', methods=['GET'])
def get_precios_monedas():
	precios_monedas = [precio_moneda.json() for precio_moneda in Precio_moneda.query.all()]
	return jsonify({'Precios monedas': precios_monedas})

@app.route('/api/precio_moneda/<id_moneda>&<fecha>', methods=['GET'])
def get_precio_moneda(id_moneda, fecha):
	precio_moneda = Precio_moneda.query.filter_by(id_moneda=id_moneda).filter_by(fecha=fecha).first()
	if precio_moneda is None:
		return jsonify({'message': 'La moneda no existe o no tiene precio en la fecha indicada'}), 404
	return jsonify({'Precio moneda': precio_moneda.json()})

@app.route('/api/precio_moneda/<id_moneda>&<fecha>', methods=['PUT'])
def update_precio_moneda(id_moneda, fecha):
	precio_moneda = Precio_moneda.query.filter_by(id_moneda=id_moneda).filter_by(fecha=fecha).first()
	if precio_moneda is None:
		return jsonify({'message': 'La moneda no existe o no tiene precio en la fecha indicada'}), 404
	json = request.get_json(force=True)
	if json.get('valor') is None:
		return jsonify({'message': 'El formato está mal'}), 400
	precio_moneda.valor = json['valor']
	precio_moneda.update()
	return jsonify({'Precio moneda': precio_moneda.json()})

@app.route('/api/precio_moneda/<id_moneda>&<fecha>', methods=['DELETE'])
def delete_precio_moneda(id_moneda, fecha):
	precio_moneda = Precio_moneda.query.filter_by(id_moneda=id_moneda).filter_by(fecha=fecha).first()
	if precio_moneda is None:
		return jsonify({'message': 'La moneda no existe o no tiene precio en la fecha indicada'}), 404
	precio_moneda.delete()
	return jsonify({'Precio moneda eliminado': precio_moneda.json()})

if __name__=='__main__':
	app.run(debug=True)